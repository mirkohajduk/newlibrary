package com.mirko.library.repository;

import com.mirko.library.model.BookCopy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookCopyRepository extends JpaRepository <BookCopy, Long> {
}
