package com.mirko.library.controller;

import com.mirko.library.dto.*;
import com.mirko.library.model.User;
import com.mirko.library.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserToUserDTOOut userToUserDTOOut;
    private final UserDTOToUser userDTOToUser;
    private final UserToUserDTO userToUserDTO;
    private final UserService userService;

    public UserController(UserToUserDTOOut userToUserDTOOut, UserDTOToUser userDTOToUser, UserToUserDTO userToUserDTO, UserService userService) {
        this.userToUserDTOOut = userToUserDTOOut;
        this.userDTOToUser = userDTOToUser;
        this.userToUserDTO = userToUserDTO;
        this.userService = userService;
    }


    @GetMapping
    @ApiOperation(value = "This option will return all users from database")
    public ResponseEntity<List<UserDTOOut>> getAllUsers() {
        return ResponseEntity.ok(userToUserDTOOut.convert(userService.findAllUsers()));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "This option will return user by matching id")
    public ResponseEntity<UserDTOOut> findUserByID(@PathVariable Long id){
        User user = userService.findByID(id);

        UserDTOOut convertedUser = userToUserDTOOut.convert(user);

        return ResponseEntity.ok(convertedUser);

    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "This option will delete user from database")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        userService.delete(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping
    @ApiOperation(value = "This option will save user")
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO newUser){
        User convertUser = userDTOToUser.convert(newUser);

        User savedUser = userService.saveUser(convertUser);

        UserDTO convertedDTOUser = userToUserDTO.convert(savedUser);

        return ResponseEntity.status(HttpStatus.CREATED).body(convertedDTOUser);

    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "This option edits user details")
    public ResponseEntity<UserDTOOut> editUser(@RequestBody UserDTO userDTO, @PathVariable Long id){
        if(!id.equals(userDTO.getId())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User editedUser = userService.saveUser(userDTOToUser.convert(userDTO));

        UserDTOOut convertedUser = userToUserDTOOut.convert(editedUser);

        return ResponseEntity.ok(convertedUser);
    }

}
