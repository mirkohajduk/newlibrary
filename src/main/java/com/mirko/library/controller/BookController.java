package com.mirko.library.controller;

import com.mirko.library.dto.BookDTO;
import com.mirko.library.dto.BookDTOToBook;
import com.mirko.library.dto.BookToBookDTO;
import com.mirko.library.model.Book;
import com.mirko.library.service.BookService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/book")
public class BookController {

    private final BookService bookService;
    private final BookToBookDTO bookToBookDTO;
    private final BookDTOToBook bookDTOToBook;


    public BookController(BookService bookService, BookToBookDTO bookToBookDTO, BookDTOToBook bookDTOToBook) {
        this.bookService = bookService;
        this.bookToBookDTO = bookToBookDTO;
        this.bookDTOToBook = bookDTOToBook;
    }

    @GetMapping
    @ApiOperation(value = "This function returns all books from database")
    public ResponseEntity<List<BookDTO>> getAllBooks() {

        return ResponseEntity.ok(bookToBookDTO.convert(bookService.findAll()));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "This function returns book from database by matching id")
    public ResponseEntity<BookDTO> getBookById(@PathVariable Long id) {
        Book book = bookService.findById(id);

        BookDTO convertedBook = bookToBookDTO.convert(book);

        return ResponseEntity.ok(convertedBook);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "This function deletes book from database by matching id")
    public ResponseEntity<Void> deleteBook(@PathVariable Long id) {
        bookService.delete(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @PostMapping(value = "/add")
    @ApiOperation(value = "This function adds book into database")
    public ResponseEntity<BookDTO> addBook(@RequestBody BookDTO newBook) {
        Book convertedNewBook = bookDTOToBook.convert(newBook);

        Book savedBook = bookService.addBook(convertedNewBook);

        BookDTO convertedSavedBook = bookToBookDTO.convert(savedBook);

        return ResponseEntity.status(HttpStatus.CREATED).body(convertedSavedBook);
    }


    @PutMapping(value = "/{id}")
    @ApiOperation(value = "This function will edit book by matching ID")
    public ResponseEntity<BookDTO> editBook(@RequestBody BookDTO oldBook, @PathVariable Long id) {
        Book editedBook = bookService.editBook(bookDTOToBook.convert(oldBook));

        BookDTO convertedNewBook = bookToBookDTO.convert(editedBook);

        return ResponseEntity.ok(convertedNewBook);
    }
}

