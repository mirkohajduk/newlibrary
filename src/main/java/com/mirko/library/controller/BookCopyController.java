package com.mirko.library.controller;

import com.mirko.library.dto.BookCopyDTO;
import com.mirko.library.dto.BookCopyToBookCopyDTO;
import com.mirko.library.model.BookCopy;
import com.mirko.library.service.BookCopyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bookcopy")
public class BookCopyController {

    private final BookCopyService bookCopyService;
    private final BookCopyToBookCopyDTO bookCopyToBookCopyDTO;

    public BookCopyController(BookCopyService bookCopyService,
                              BookCopyToBookCopyDTO bookCopyToBookCopyDTO) {
        this.bookCopyService = bookCopyService;
        this.bookCopyToBookCopyDTO = bookCopyToBookCopyDTO;

    }

    @GetMapping
    @ApiOperation(value = "This function returns all book copies from database")
    public ResponseEntity<List<BookCopyDTO>> getAllCopies() {
        return ResponseEntity.ok(bookCopyToBookCopyDTO.convert(bookCopyService.finAll()));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "This function return book copy by provided id")
    public ResponseEntity<BookCopyDTO> findById(@PathVariable Long id) {
        BookCopy bookCopy = bookCopyService.findById(id);

        BookCopyDTO convertedBookCopy = bookCopyToBookCopyDTO.convert(bookCopy);

        return ResponseEntity.ok(convertedBookCopy);
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "This option will add a book copy into database. You can provide only ID")
    public ResponseEntity<BookCopyDTO> saveBookCopy(Long id) {
        BookCopyDTO savedBook = bookCopyToBookCopyDTO.convert(bookCopyService.saveBookCopy(id));

        return ResponseEntity.status(HttpStatus.CREATED).body(savedBook);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "This option will delete a book copy from database")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        bookCopyService.delete(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
