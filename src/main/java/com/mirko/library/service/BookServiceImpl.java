package com.mirko.library.service;

import com.mirko.library.model.Book;
import com.mirko.library.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * @return returns all books from database
     */
    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book findById(Long id) {
        return bookRepository.getOne(id);
    }

    @Override
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public Book addBook(Book newBook) {
        return bookRepository.save(newBook);
    }

    @Override
    public Book editBook(Book book) {
        Book editedBook = findById(book.getId());
        editedBook.setTitle(book.getTitle());
        editedBook.setAuthorName(book.getAuthorName());
        editedBook.setPublisher(book.getPublisher());
        editedBook.setDateOfPublishing(book.getDateOfPublishing());
        return bookRepository.save(editedBook);
    }


}
