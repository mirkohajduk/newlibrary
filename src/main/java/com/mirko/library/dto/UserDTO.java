package com.mirko.library.dto;

public class UserDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String email;

    public UserDTO() {
    }

    public static class Builder {
        private Long id;
        private String firstName;
        private String lastName;
        private String userName;
        private String password;
        private String email;

        public Builder () {}

        public Builder withID(Long id){
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public Builder withUserName(String userName){
            this.userName = userName;
            return this;
        }

        public Builder withPassword(String password){
            this.password = password;
            return this;
        }

        public Builder withEmail(String email){
            this.email = email;
            return this;
        }

        public UserDTO build(){
            UserDTO userDTO = new UserDTO();
            userDTO.id = this.id;
            userDTO.firstName = this.firstName;
            userDTO.lastName = this.lastName;
            userDTO.userName = this.userName;
            userDTO.password = this.password;
            userDTO.email = this.email;

            return userDTO;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
