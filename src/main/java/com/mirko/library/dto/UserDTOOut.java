package com.mirko.library.dto;

import java.util.Objects;

public class UserDTOOut {

    private Long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;

    public UserDTOOut() {
    }

    public static class Builder{

        private Long id;
        private String firstName;
        private String lastName;
        private String userName;
        private String email;

        public Builder ()  {}

        public Builder withID(Long id){
            this.id = id;
            return this;
        }

        public Builder withFirstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public Builder withUserName(String userName){
            this.userName = userName;
            return this;
        }

        public Builder withEmail(String email){
            this.email = email;
            return this;
        }

        public UserDTOOut build(){
            UserDTOOut userDTOOut = new UserDTOOut();
            userDTOOut.id = this.id;
            userDTOOut.firstName = this.firstName;
            userDTOOut.lastName = this.lastName;
            userDTOOut.userName = this.userName;
            userDTOOut.email = this.email;

            return userDTOOut;
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserDTOOut{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTOOut that = (UserDTOOut) o;
        return id.equals(that.id) &&
                firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                userName.equals(that.userName) &&
                email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, userName, email);
    }
}
