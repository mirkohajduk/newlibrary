package com.mirko.library.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class BookDTO {

    private Long id;
    private String title;
    private String authorName;
    private String publisher;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateOfPublishing;

    public BookDTO() {
    }

    public static class Builder{

        private Long id;
        private String title;
        private String authorName;
        private String publisher;
        @JsonFormat(pattern = "dd-MM-yyyy")
        private LocalDate dateOfPublishing;

        public Builder () {}

        public Builder withID(Long id) {
            this.id = id;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withAuthorName(String authorName) {
            this.authorName = authorName;
            return this;
        }

        public Builder withPublisher(String publisher) {
            this.publisher = publisher;
            return this;
        }
        public Builder withDateOfPublishing(LocalDate dateOfPublishing) {
            this.dateOfPublishing = dateOfPublishing;
            return this;
        }

        public BookDTO build() {
            BookDTO bookDTO = new BookDTO();
            bookDTO.id = this.id;
            bookDTO.title = this.title;
            bookDTO.authorName = this.authorName;
            bookDTO.publisher = this.publisher;
            bookDTO.dateOfPublishing = this.dateOfPublishing;

            return bookDTO;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public LocalDate getDateOfPublishing() {
        return dateOfPublishing;
    }

    public void setDateOfPublishing(LocalDate dateOfPublishing) {
        this.dateOfPublishing = dateOfPublishing;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", authorName='" + authorName + '\'' +
                ", publisher='" + publisher + '\'' +
                ", dateOfPublishing=" + dateOfPublishing +
                '}';
    }
}
