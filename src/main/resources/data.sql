insert into books (author_name, date_of_publishing, publisher, title)
values
('Miljneko Cutuk', '2020-05-05', 'Laguna', 'mars'),
('Marinko Rokvic', '1978-09-12', 'Juzni vetar', 'Zapevajmo Slozno'),
('Nikola Zigic', '1998-11-28', 'Plava Ptica', 'Strategija Kontre');

INSERT INTO bookcopyies (id, serial_number, book_id)
VALUES
(1, '563281ff-de00-4d28-9f56-49426cf668bf', 1),
(2, '2437d313-9a66-4ff1-bc15-fee139913dfe', 2),
(3, '54f7aac4-746b-40e1-a461-7ffda76633c0', 3);

INSERT INTO users (id, email, first_name, last_name, password, user_name)
VALUES
(1, 'hello@gmail.com', 'Nikola', 'Zigic', 'hello', 'nzigic'),
(2, 'millo@yahoo.com', 'Marko', 'Milic', 'milyo', 'mmilic');